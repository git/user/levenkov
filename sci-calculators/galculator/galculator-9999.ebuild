# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sci-calculators/galculator/galculator-2.1.3.ebuild,v 1.3 2014/05/04 12:12:27 pacho Exp $

EAPI=5
GCONF_DEBUG=no

inherit gnome2
inherit subversion

DESCRIPTION="GTK+ based algebraic and RPN calculator"
HOMEPAGE="http://galculator.sourceforge.net/"
SRC_URI=""

ESVN_REPO_URI="svn://svn.code.sf.net/p/${PN}/code/trunk/${PN}"
ESVN_BOOTSTRAP="autogen.sh"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="x86"
IUSE="-quadmath"

RDEPEND="
	dev-libs/glib:2
	x11-libs/gtk+:3
	x11-libs/pango
"
DEPEND="${RDEPEND}
	dev-util/intltool
	sys-devel/flex
	sys-devel/gettext
	virtual/pkgconfig
"

src_unpack() {
	subversion_src_unpack
}

src_configure() {
	gnome2_src_configure \
		$(use_enable quadmath)
}

DOCS="AUTHORS ChangeLog NEWS README THANKS doc/shortcuts"
